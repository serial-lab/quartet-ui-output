"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.EndpointsList = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; // Copyright (c) 2018 SerialLab Corp.
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

var _output = require("../../reducers/output");

const React = qu4rtet.require("react");
const { Component } = React;
const { RightPanel } = qu4rtet.require("./components/layouts/Panels");
const { connect } = qu4rtet.require("react-redux");
const { FormattedMessage } = qu4rtet.require("react-intl");
const { PaginatedList } = qu4rtet.require("./components/elements/PaginatedList");
const { DeleteObject } = qu4rtet.require("./components/elements/DeleteObject");

const EndpointsTableHeader = props => React.createElement(
  "thead",
  { style: { textAlign: "center", verticalAlign: "middle" } },
  React.createElement(
    "tr",
    null,
    React.createElement(
      "th",
      null,
      " ",
      React.createElement(FormattedMessage, { id: "plugins.output.id" })
    ),
    React.createElement(
      "th",
      null,
      " ",
      React.createElement(FormattedMessage, { id: "plugins.output.name" })
    ),
    React.createElement(
      "th",
      null,
      " ",
      React.createElement(FormattedMessage, { id: "plugins.output.urn" })
    )
  )
);

const EndpointsEntry = props => {
  const goTo = path => {
    props.history.push(path);
  };
  const goToPayload = goTo.bind(undefined, {
    pathname: `/output/${props.server.serverID}/add-endpoint`,
    state: { defaultValues: props.entry, edit: true }
  });
  let deleteObj = DeleteObject ? React.createElement(DeleteObject, {
    entry: props.entry,
    operationId: "output_end_points_delete",
    server: props.server,
    title: React.createElement(FormattedMessage, { id: "plugins.output.deleteEndpointConfirm" }),
    body: React.createElement(FormattedMessage, { id: "plugins.output.deleteEndpointConfirmBody" }),
    postDeleteAction: props.loadEntries
  }) : null;
  return React.createElement(
    "tr",
    { key: props.entry.id },
    React.createElement(
      "td",
      { onClick: goToPayload },
      props.entry.id
    ),
    React.createElement(
      "td",
      { onClick: goToPayload },
      props.entry.name
    ),
    React.createElement(
      "td",
      { onClick: goToPayload },
      props.entry.urn
    ),
    React.createElement(
      "td",
      null,
      deleteObj
    )
  );
};

class _EndpointsList extends Component {
  render() {
    const { server, endpoints, loadEndpoints, count, next } = this.props;
    return React.createElement(
      RightPanel,
      {
        title: React.createElement(FormattedMessage, { id: "plugins.output.endpointsList" }) },
      React.createElement(
        "div",
        { className: "large-cards-container full-large" },
        React.createElement(PaginatedList, _extends({}, this.props, {
          listTitle: React.createElement(FormattedMessage, { id: "plugins.output.endpointsList" }),
          history: this.props.history,
          loadEntries: loadEndpoints,
          server: server,
          entries: endpoints,
          entryClass: EndpointsEntry,
          tableHeaderClass: EndpointsTableHeader,
          count: count,
          next: next
        }))
      )
    );
  }
}

const EndpointsList = exports.EndpointsList = connect((state, ownProps) => {
  const isServerSet = () => {
    return state.output.servers && state.output.servers[ownProps.match.params.serverID];
  };
  return {
    server: state.serversettings.servers[ownProps.match.params.serverID],
    endpoints: isServerSet() ? state.output.servers[ownProps.match.params.serverID].endpoints : [],
    count: isServerSet() ? state.output.servers[ownProps.match.params.serverID].count : 0,
    next: isServerSet() ? state.output.servers[ownProps.match.params.serverID].next : null
  };
}, { loadEndpoints: _output.loadEndpoints })(_EndpointsList);