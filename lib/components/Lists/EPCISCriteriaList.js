"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.EPCISCriteriaList = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; // Copyright (c) 2018 SerialLab Corp.
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

var _output = require("../../reducers/output");

const React = qu4rtet.require("react");
const { Component } = React;
const { RightPanel } = qu4rtet.require("./components/layouts/Panels");
const { connect } = qu4rtet.require("react-redux");
const { FormattedMessage } = qu4rtet.require("react-intl");
const { PaginatedList } = qu4rtet.require("./components/elements/PaginatedList");
const { DeleteObject } = qu4rtet.require("./components/elements/DeleteObject");

const EPCISCriteriaTableHeader = props => React.createElement(
  "thead",
  { style: { textAlign: "center", verticalAlign: "middle" } },
  React.createElement(
    "tr",
    null,
    React.createElement(
      "th",
      null,
      " ",
      React.createElement(FormattedMessage, { id: "plugins.output.id" })
    ),
    React.createElement(
      "th",
      null,
      " ",
      React.createElement(FormattedMessage, { id: "plugins.output.name" })
    ),
    React.createElement(
      "th",
      null,
      " ",
      React.createElement(FormattedMessage, { id: "plugins.output.endpoint" })
    ),
    React.createElement(
      "th",
      null,
      " ",
      React.createElement(FormattedMessage, { id: "plugins.output.authenticationInfo" })
    ),
    React.createElement(
      "th",
      null,
      " ",
      React.createElement(FormattedMessage, { id: "plugins.output.bizStep" })
    ),
    React.createElement(
      "th",
      null,
      " ",
      React.createElement(FormattedMessage, { id: "plugins.output.bizLocation" })
    ),
    React.createElement(
      "th",
      null,
      " ",
      React.createElement(FormattedMessage, { id: "plugins.output.sourceType" })
    ),
    React.createElement(
      "th",
      null,
      " ",
      React.createElement(FormattedMessage, { id: "plugins.output.destinationType" })
    )
  )
);

const EPCISCriteriaEntry = props => {
  const goTo = path => {
    props.history.push(path);
  };
  const goToPayload = goTo.bind(undefined, {
    pathname: `/output/${props.server.serverID}/add-criteria`,
    state: { defaultValues: props.entry, edit: true }
  });
  let deleteObj = DeleteObject ? React.createElement(DeleteObject, {
    entry: props.entry,
    operationId: "output_epcis_output_criteria_delete",
    server: props.server,
    title: React.createElement(FormattedMessage, { id: "plugins.output.deleteCriteriaConfirm" }),
    body: React.createElement(FormattedMessage, { id: "plugins.output.deleteCriteriaConfirmBody" }),
    postDeleteAction: props.loadEntries
  }) : null;
  return React.createElement(
    "tr",
    { key: props.entry.id },
    React.createElement(
      "td",
      { onClick: goToPayload },
      props.entry.id
    ),
    React.createElement(
      "td",
      { onClick: goToPayload },
      props.entry.name
    ),
    React.createElement(
      "td",
      { onClick: goToPayload },
      props.entry.end_point ? props.entry.end_point.name : null
    ),
    React.createElement(
      "td",
      { onClick: goToPayload },
      props.entry.authentication_info ? props.entry.authentication_info.type : null
    ),
    React.createElement(
      "td",
      { onClick: goToPayload },
      props.entry.biz_step
    ),
    React.createElement(
      "td",
      { onClick: goToPayload },
      props.entry.biz_location
    ),
    React.createElement(
      "td",
      { onClick: goToPayload },
      props.entry.source_type
    ),
    React.createElement(
      "td",
      { onClick: goToPayload },
      props.entry.destination_type
    ),
    React.createElement(
      "td",
      null,
      deleteObj
    )
  );
};

class _EPCISCriteriaList extends Component {
  render() {
    const { server, criteria, loadEPCISCriteria, count, next } = this.props;
    return React.createElement(
      RightPanel,
      {
        title: React.createElement(FormattedMessage, { id: "plugins.output.EPCISOutputCriteriaList" }) },
      React.createElement(
        "div",
        { className: "large-cards-container full-large" },
        React.createElement(PaginatedList, _extends({}, this.props, {
          listTitle: React.createElement(FormattedMessage, { id: "plugins.output.EPCISOutputCriteriaList" }),
          history: this.props.history,
          loadEntries: loadEPCISCriteria,
          server: server,
          entries: criteria,
          entryClass: EPCISCriteriaEntry,
          tableHeaderClass: EPCISCriteriaTableHeader,
          count: count,
          next: next
        }))
      )
    );
  }
}

const EPCISCriteriaList = exports.EPCISCriteriaList = connect((state, ownProps) => {
  const isServerSet = () => {
    return state.output.servers && state.output.servers[ownProps.match.params.serverID];
  };
  return {
    server: state.serversettings.servers[ownProps.match.params.serverID],
    criteria: isServerSet() ? state.output.servers[ownProps.match.params.serverID].criteria : [],
    count: isServerSet() ? state.output.servers[ownProps.match.params.serverID].count : 0,
    next: isServerSet() ? state.output.servers[ownProps.match.params.serverID].next : null
  };
}, { loadEPCISCriteria: _output.loadEPCISCriteria })(_EPCISCriteriaList);